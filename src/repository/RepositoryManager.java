package repository;

import entity.Entity;

import java.util.HashMap;

public class RepositoryManager {
    private static RepositoryManager instance = null;
    private final HashMap<String, Repository<? extends Entity>> repositories = new HashMap<>();

    public <T extends Entity> Repository<T> getRepository(Class<T> clazz) {
        Repository<? extends Entity> repository = repositories.get(clazz.getName());
        if (repository == null) {
            Repository<T> newRepo = new Repository<>(clazz.getName());
            repositories.put(clazz.getName(), newRepo);
            return newRepo;
        }
        return (Repository<T>) repository;
    }

    private RepositoryManager() {
    }

    public static RepositoryManager getInstance() {
        if (instance == null) {
            instance = new RepositoryManager();
        }
        return instance;
    }
}

package repository;

import entity.Entity;
import util.properties.PropertiesManager;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import static util.logger.Logger.error;


public class Repository<T extends Entity> {
    private final String mainDirectory;
    private final String sequenceFilePath;
    private Integer nextId;
    private final SecretKey key;

    protected Repository(String entityDirName) {
        mainDirectory = PropertiesManager.getInstance().getProperties("dbPath") + entityDirName;
        File dir = new File(mainDirectory);
        dir.mkdir();
        sequenceFilePath = mainDirectory + "/sequence/sequence.seq";
        nextId = readSequence();

        key = new SecretKeySpec(PropertiesManager.getInstance().getProperties("key").getBytes(), "AES");
    }

    private int readSequence() {
        File file = new File(sequenceFilePath);
        if (!file.exists()) {
            File dir = new File(mainDirectory + "/sequence");
            dir.mkdir();
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(sequenceFilePath))) {
                int value = 0;
                writer.write(String.valueOf(value));
                return value;
            } catch (IOException e) {
                error("error during create sequence file", e);
            }
        } else {
            try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                return Integer.parseInt(reader.readLine());
            } catch (IOException e) {
                error("error during read sequence from file", e);
            }
        }
        return -1;
    }

    private void writeSequence() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(sequenceFilePath))) {
            writer.write(String.valueOf(nextId + 1));
            nextId++;
        } catch (IOException e) {
            error("error during write sequence", e);
        }
    }

    private int getNextId() {
        int currentId = nextId;
        writeSequence();
        return currentId;
    }

    private String getFilename(int id) {
        return mainDirectory + File.separator + id + ".txt";
    }

    public int save(T entity) {
        int id = getNextId();
        entity.setId(id);
        String fileName = getFilename(id);
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
            oos.writeObject(entity);
            return id;
        } catch (IOException e) {
            error("error during save entity", e);
        }
        return -1;
    }

    public T findById(int id) {
        String fileName = getFilename(id);
        try (ObjectInputStream oos = new ObjectInputStream(new FileInputStream(fileName))) {
            return (T) oos.readObject();
        } catch (IOException | ClassNotFoundException e) {
            error("error during find by id entity", e);
        }
        return null;
    }

    public List<T> findAll() {
        List<T> result = new ArrayList<>();
        File dir = new File(mainDirectory);
        for (File file : dir.listFiles()) {
            if (file.isFile() && file.getName().endsWith(".txt")) {
                try (ObjectInputStream oos = new ObjectInputStream(new FileInputStream(file))) {
                    result.add((T) oos.readObject());
                } catch (IOException | ClassNotFoundException e) {
                    error("error during find all entity", e);
                }
            }
        }
        return result;
    }

    public boolean deleteById(int id) {
        String fileName = getFilename(id);
        File file = new File(fileName);
        if (file.exists()) {
            return file.delete();
        }
        return false;
    }

    public boolean update(T object) {
        String fileName = getFilename(object.getId());
        File file = new File(fileName);
        if (file.exists() && file.delete()) {
            try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
                oos.writeObject(object);
                return true;
            } catch (IOException e) {
                error("error during update entity", e);
            }
        }
        return false;
    }

}

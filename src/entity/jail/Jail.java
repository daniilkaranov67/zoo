package entity.jail;

import entity.Entity;
import entity.animal.Animal;

import java.util.Set;

public class Jail implements Entity {
    private Integer id;
    private Set<Animal.Type> types;
    private int capacity;

    public Jail() {

    }

    public Jail(Integer id, Set<Animal.Type> types, int capacity) {
        this.id = id;
        this.types = types;
        this.capacity = capacity;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Jail{" +
                "id=" + id +
                ", types=" + types +
                ", capacity=" + capacity +
                '}';
    }

    public int getCapacity() {
        return capacity;
    }

    public Set<Animal.Type> getTypes() {
        return types;
    }
}

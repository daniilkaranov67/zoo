package entity.customer;

import entity.Entity;

import java.util.Date;
import java.util.UUID;

public class Ticket implements Entity {

    private final int price;
    private final String code;
    private final Date createdDate;
    private Integer id;
    private boolean isActive = true;
    private Date visitedDate;
    private final Integer customerId;

    public Ticket(int price, Integer customerId) {
        this.customerId = customerId;
        this.code = UUID.randomUUID().toString();
        this.price = price;
        createdDate = new Date();
    }

    public void useTicket() {
        visitedDate = new Date();
        isActive = false;
    }

    public int getPrice() {
        return price;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public String getCode() {
        return code;
    }

    public boolean isActive() {
        return isActive;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Date getVisitedDate() {
        return visitedDate;
    }


    public String getBoughtInformation() {
        return "Цена: " + price + " , " + "id: " + code + " , " + "дата покупки: " + createdDate;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "price=" + price +
                ", code='" + code + '\'' +
                ", isActive=" + isActive +
                ", createdDate=" + createdDate +
                ", visitedDate=" + visitedDate +
                '}';
    }
}

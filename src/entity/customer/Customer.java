package entity.customer;

import entity.Entity;

import java.util.Objects;

public class Customer implements Entity {
    private String name;
    private Integer id;
    private final int age;
    private final String phoneNumb;

    public Customer(String name, int age, String phoneNumb) {
        this.name = name;
        this.age = age;
        this.phoneNumb = phoneNumb;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhoneNumb() {
        return phoneNumb;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(phoneNumb, customer.phoneNumb);
    }

    @Override
    public int hashCode() {
        return Objects.hash(phoneNumb);
    }
}

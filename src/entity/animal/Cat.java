package entity.animal;

public class Cat extends Animal {

    public Cat(String name, Integer jailId) {
        super(name, Type.CAT, jailId);
        System.out.println();
    }

    @Override
    public void say() {
        System.out.println("meow");
    }

    public String method() {
        return "a";
    }


}

package entity.animal;

import entity.animal.Animal;

public class Tiger extends Animal {
    public Tiger(String name, Integer jailId) {
        super(name, Type.TIGER, jailId);
    }

    @Override
    public void say() {
        System.out.println("rrr");
    }
}

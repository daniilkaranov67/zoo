package entity.animal;

import entity.Entity;

import java.util.Objects;

public abstract class Animal implements Entity {
    public enum Type {
        CAT,
        DOG,
        TIGER
    }

    protected boolean isActive = true;
    private String name;
    private Integer id;
    private Integer jailId;
    private final Type type;

    public final void sleep() {
        isActive = false;
    }

    public final void wakeUp() {
        isActive = true;
    }

    public Animal(String name, Type type, Integer jailId) {
        this.jailId = jailId;
        this.name = name;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public abstract void say();

    public Integer getJailId() {
        return jailId;
    }

    public void setJailId(Integer jailId) {
        this.jailId = jailId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return id.equals(animal.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Animal{" +
                "имя='" + name + '\'' +
                ", id=" + id +
                ", тип=" + type +
                '}';
    }
}

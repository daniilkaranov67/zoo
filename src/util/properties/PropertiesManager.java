package util.properties;

import util.logger.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesManager {
    private static PropertiesManager instance = null;
    private final Properties properties = new Properties();

    private PropertiesManager() {
        try {
            InputStream inputStream = new FileInputStream("application.properties");
            properties.load(inputStream);
        } catch (IOException e) {
            Logger.error("error during read application.properties", e);
        }
    }

    public static PropertiesManager getInstance() {
        if (instance == null) {
            instance = new PropertiesManager();
        }
        return instance;
    }

    public String getProperties(String key) {
        return properties.getProperty(key);
    }
}

package util.logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

public class Logger {
    private static final String ERROR = "error";
    private static final String WARN = "warn";
    private static final String INFO = "info";
    private static final String FILE_NAME = "./log.txt";

    public static void error(String message, Throwable t) {
        log(ERROR, message, t);
    }

    public static void info(String message) {
        log(INFO, message, null);
    }

    public static void warn(String message) {
        log(WARN, message, null);
    }

    private static void log(String level, String msg, Throwable t) {
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(FILE_NAME, true)))) {
            out.println(LocalDateTime.now() + " [" + level + "] " + "in thread [" + Thread.currentThread().getName() + "] " + msg);
            if (t != null) {
                out.println("Stack trace:");
                t.printStackTrace(out);
            }
        } catch (IOException e) {
            System.out.println("Код ошибки 999");
        }
    }
}

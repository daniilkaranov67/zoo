package service;

import entity.animal.Animal;
import entity.jail.Jail;
import repository.Repository;
import repository.RepositoryManager;

public class ZooService {
    private final Repository<Jail> jailRepository = RepositoryManager.getInstance().getRepository(Jail.class);
    private final Repository<Animal> animalRepository = RepositoryManager.getInstance().getRepository(Animal.class);

    public ZooService() {
    }

    public void addAnimal(Animal animal) {
        Jail jail = jailRepository.findById(animal.getJailId());
        if (jail != null) {
            if (animalRepository.findAll().stream().filter(a -> a.getJailId().equals(animal.getJailId())).count() < jail.getCapacity()) {
                if (jail.getTypes().contains(animal.getType())) {
                    int id = animalRepository.save(animal);
                    if (id != -1) {
                        System.out.println("Добавлено новое животное, его id: " + id);
                    } else {
                        System.out.println("Не удалось сохранить животное в базе данных");
                    }
                } else {
                    System.out.println("Животное с типом: " + animal.getType() + " нельзя добавить в данную клетку!");
                }
            } else {
                System.out.println("Не удалось добавить животное, клетка заполнена!");
            }
        } else {
            System.out.println("Нет клетки с id: " + animal.getJailId());
        }
    }

    public void printAllAnimals() {
        boolean isZooEmpty = true;
        for (Animal animal : animalRepository.findAll()) {
            if (animal != null) {
                System.out.print(animal + " ");
                isZooEmpty = false;
            }
        }
        if (isZooEmpty) {
            System.out.println("В зоопарке еще нет животных");
        } else {
            System.out.println();
        }
    }

    public void printAllJails() {
        StringBuilder jailsStr = new StringBuilder("Клетки:\n");
        for (Jail jail : jailRepository.findAll()) {
            jailsStr.append("Id: ").append(jail.getId()).append(", типы: ").append(jail.getTypes()).append("\n");
        }
        System.out.println(jailsStr.toString());
    }
}

package service;

import entity.customer.Customer;
import entity.customer.Ticket;
import repository.Repository;
import repository.RepositoryManager;
import util.struct.Pair;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TicketService {

    private final Repository<Customer> customerRepository = RepositoryManager.getInstance().getRepository(Customer.class);
    private final Repository<Ticket> ticketRepository = RepositoryManager.getInstance().getRepository(Ticket.class);

    public Ticket generateTicket(String phoneNumb, String name, int age) {
        Optional<Customer> maybeCustomer = customerRepository.findAll()
                .stream().filter(customer -> customer.getPhoneNumb().equals(phoneNumb)).findFirst();
        if (maybeCustomer.isPresent()) {
            Customer customer = maybeCustomer.get();
            Ticket newTicket = generateTicket(age, customer.getId());
            if (ticketRepository.save(newTicket) != -1) {
                return newTicket;
            }
        } else {
            int id = customerRepository.save(new Customer(name, age, phoneNumb));
            if (id != -1) {
                Ticket newTicket = generateTicket(age, id);
                if (ticketRepository.save(newTicket) != -1) {
                    return newTicket;
                }
            }
        }
        return null;
    }

    public boolean useTicket(String code) {
        Optional<Ticket> maybeTicket = ticketRepository.findAll()
                .stream()
                .filter(ticket -> ticket.getCode().equals(code) && ticket.isActive()).findAny();

        if (maybeTicket.isPresent()) {
            Ticket ticket = maybeTicket.get();
            ticket.useTicket();
            return ticketRepository.update(ticket);
        }
        return false;
    }

    public String getCustomersInfo() {
        StringBuilder builder = new StringBuilder();
        for (Customer customer : customerRepository.findAll()) {
            builder.append("Билеты пользователя ").append(customer.getPhoneNumb()).append(" :\n");
            for (Ticket ticket : ticketRepository.findAll().stream()
                    .filter(ticket -> ticket.getCustomerId().equals(customer.getId())).collect(Collectors.toList())) {
                builder.append(ticket).append("\n");
            }
        }
        return builder.toString();
    }

    /**
     * Ищет покупателя по номеру телефона
     *
     * @param phoneNumb номер телефона
     * @return возвращает покупателя или null;
     */
    private Pair<Customer, List<Ticket>> findPairByPhoneNumb(String phoneNumb) {
        Optional<Customer> maybeCustomer = customerRepository.findAll()
                .stream().filter(customer -> customer.getPhoneNumb().equals(phoneNumb)).findFirst();
        if (maybeCustomer.isPresent()) {
            Customer customer = maybeCustomer.get();
            List<Ticket> tickets = ticketRepository.findAll()
                    .stream()
                    .filter(ticket -> ticket.getCustomerId().equals(customer.getId()))
                    .collect(Collectors.toList());
            return Pair.of(customer, tickets);
        }
        return null;
    }

    private Ticket generateTicket(int age, Integer customerId) {
        if (age < 10) {
            return new Ticket(100, customerId);
        } else if (age < 18) {
            return new Ticket(200, customerId);
        } else {
            return new Ticket(300, customerId);
        }
    }
}

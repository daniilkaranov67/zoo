package command.impl;

import command.Command;
import service.ZooService;

public class GetJailsCommand implements Command {

    private final ZooService zooService;

    public GetJailsCommand(ZooService zooService) {
        this.zooService = zooService;
    }

    @Override
    public void execute(String[] args) {
        zooService.printAllJails();
    }

    @Override
    public String getInstruction() {
        return "Выводит все клетки";
    }
}

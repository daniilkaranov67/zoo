package command.impl;

import command.Command;
import service.ZooService;

public class PrintAllAnimalsCommand implements Command {

    private final ZooService zooService;

    public PrintAllAnimalsCommand(ZooService zooService) {
        this.zooService = zooService;
    }

    @Override
    public void execute(String[] args) {
        zooService.printAllAnimals();
    }

    @Override
    public String getInstruction() {
        return "Печатает всех животных";
    }
}

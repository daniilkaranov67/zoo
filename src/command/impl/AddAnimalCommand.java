package command.impl;

import entity.animal.Animal;
import entity.animal.Cat;
import entity.animal.Dog;
import entity.animal.Tiger;
import command.Command;
import service.ZooService;

public class AddAnimalCommand implements Command {

    private final ZooService zooService;

    public AddAnimalCommand(ZooService zooService) {
        this.zooService = zooService;
    }

    @Override
    public void execute(String[] args) {
        if (args.length == 3) {
            String name = args[0];
            Animal.Type type = Animal.Type.valueOf(args[1]);
            int jailId = Integer.parseInt(args[2]);
            if (type == Animal.Type.CAT) {
                zooService.addAnimal(new Cat(name, jailId));
            } else if (type == Animal.Type.TIGER) {
                zooService.addAnimal(new Tiger(name, jailId));
            } else if (type == Animal.Type.DOG) {
                zooService.addAnimal(new Dog(name, jailId));
            }
        } else {
            System.out.println("Аргументов должно быть 3");
            System.out.println(getInstruction());
        }
    }

    @Override
    public String getInstruction() {
        return "Первый аргумент команды - имя, второй аргумент - команды тип животного, третий аргумент - id клетки";
    }
}

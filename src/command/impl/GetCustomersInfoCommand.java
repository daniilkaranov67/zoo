package command.impl;

import command.Command;
import service.TicketService;

public class GetCustomersInfoCommand implements Command {

    private final TicketService ticketService;

    public GetCustomersInfoCommand(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @Override
    public void execute(String[] args) {
        System.out.println(ticketService.getCustomersInfo());
    }

    @Override
    public String getInstruction() {
        return "Печатает информацию о клиентах";
    }
}

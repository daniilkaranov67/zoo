package command.impl;

import command.Command;
import entity.customer.Ticket;
import service.TicketService;

public class GenerateTicketCommand implements Command {

    private final TicketService ticketService;

    public GenerateTicketCommand(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @Override
    public void execute(String[] args) {
        if (args.length == 3) {
            Ticket ticket = ticketService.generateTicket(args[0], args[1], Integer.parseInt(args[2]));
            if (ticket != null) {
                System.out.println(ticket.getBoughtInformation());
            } else {
                System.out.println("Не удалось создать билет");
            }
        } else {
            System.out.println("Аргументов должно быть 3");
            System.out.println(getInstruction());
        }
    }

    @Override
    public String getInstruction() {
        return "Создает билет. Первый аргумент команды - это телефон, второй - имя, третий - возраст.";
    }
}

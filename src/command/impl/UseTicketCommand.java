package command.impl;

import command.Command;
import service.TicketService;

public class UseTicketCommand implements Command {
    private final TicketService ticketService;

    public UseTicketCommand(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @Override
    public void execute(String[] args) {
        if (args.length == 1) {
            if (ticketService.useTicket(args[0])) {
                System.out.println("Вы использовали билет");
            } else {
                System.out.println("Билет с данным кодом не найден!");
            }
        } else {
            System.out.println("Аргументов должно быть 1");
            System.out.println(getInstruction());
        }
    }

    @Override
    public String getInstruction() {
        return "Использует билет. Первый аргумент код билета.";
    }
}

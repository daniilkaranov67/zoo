package command;

import util.struct.Pair;

import java.util.List;

public class CommandManager {

    private final List<Pair<String, Command>> namesAndCommandsList;

    public CommandManager(List<Pair<String, Command>> namesAndCommandsList) {
        this.namesAndCommandsList = namesAndCommandsList;
    }

    public void executeCommand(String command) {
        String[] words = command.split(" ");
        String commandName = words[0];
        String[] arguments = new String[0];
        if (words.length > 1) {
            arguments = new String[words.length - 1];
            for (int i = 1; i < words.length; i++) {
                arguments[i - 1] = words[i];
            }
        }
        for (Pair<String, Command> pair : namesAndCommandsList) {
            if (pair.getLeft().equals(commandName)) {
                pair.getRight().execute(arguments);
            }
        }
    }

    public void printAllCommands() {
        for (Pair<String, Command> pair : namesAndCommandsList) {
            System.out.println(pair.getLeft() + " : " + pair.getRight().getInstruction());
        }
    }
}

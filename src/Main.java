import command.Command;
import command.CommandManager;
import command.impl.AddAnimalCommand;
import command.impl.GenerateTicketCommand;
import command.impl.GetCustomersInfoCommand;
import command.impl.GetJailsCommand;
import command.impl.PrintAllAnimalsCommand;
import command.impl.UseTicketCommand;
import service.TicketService;
import entity.animal.Animal.Type;
import entity.jail.Jail;
import repository.Repository;
import repository.RepositoryManager;
import util.struct.Pair;
import service.ZooService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;

import static entity.animal.Animal.Type.CAT;
import static entity.animal.Animal.Type.DOG;
import static entity.animal.Animal.Type.TIGER;

public class Main {
    static boolean isRunning = true;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        initJails();
        CommandManager commandManager = createCommandManager();

        commandManager.printAllCommands();
        while (isRunning) {
            String newLine = scanner.nextLine();
            commandManager.executeCommand(newLine);
        }
    }

    private static CommandManager createCommandManager() {
        ZooService zooService = new ZooService();
        TicketService ticketService = new TicketService();
        return new CommandManager(List.of(
                Pair.of("/addAnimal", new AddAnimalCommand(zooService)),
                Pair.of("/printAllAnimals", new PrintAllAnimalsCommand(zooService)),
                Pair.of("/exit", new Command() {
                    @Override
                    public void execute(String[] args) {
                        isRunning = false;
                    }

                    @Override
                    public String getInstruction() {
                        return "Завершает программу";
                    }

                }),
                Pair.of("/getAnimalTypes", new Command() {
                    @Override
                    public void execute(String[] args) {
                        System.out.println(Arrays.toString(Type.values()));
                    }

                    @Override
                    public String getInstruction() {
                        return "Выводит все типы животных";
                    }
                }),
                Pair.of("/generateTicket", new GenerateTicketCommand(ticketService)),
                Pair.of("/useTicket", new UseTicketCommand(ticketService)),
                Pair.of("/getCustomersInfo", new GetCustomersInfoCommand(ticketService)),
                Pair.of("/getJails", new GetJailsCommand(zooService))
        ));
    }

    private static void initJails() {
        Repository<Jail> repository = RepositoryManager.getInstance().getRepository(Jail.class);
        List<Jail> jails = repository.findAll();
        Optional<Jail> maybeCatTigerJail = jails
                .stream()
                .filter(jail -> jail.getTypes().contains(TIGER) && jail.getTypes().contains(CAT))
                .findAny();
        if (maybeCatTigerJail.isEmpty()) {
            repository.save(new Jail(null, Set.of(CAT, TIGER), 2));
        }
        Optional<Jail> maybeDogJail = jails.stream().filter(jail -> jail.getTypes().contains(DOG)).findAny();
        if (maybeDogJail.isEmpty()) {
            repository.save(new Jail(null, Set.of(DOG), 2));
        }

    }
}
